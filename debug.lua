--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--
-- Debug Print Library
--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--

-- Set to false to turn off debugging
--local hy_debug = true
local hy_debug = false

function dbug(wut)
	if hy_debug then
		return print(string.format("|cffffd700>|r (|cff99ffffHY|r) |cffffd700%s|r", wut))
	else
		return nil
	end
end

--function nop()
--     Doesn't do anything.
--    return
--end