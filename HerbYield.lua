--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--
-- Patch: 7.1.5
--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--
SLASH_HERBYIELD_DUMP1 	= '/hydump'
SLASH_HERBYIELD_DUMP2 	= '/hd'

-- Set up a dummy frame and event notifications
local hyFrame = CreateFrame("Frame")
hyFrame.name = "HerbYield"

-- we're listening to ...
hyFrame:RegisterEvent('PLAYER_LOGIN')
hyFrame:RegisterEvent('PLAYER_ENTERING_WORLD')

if not hy_countBase then
    hyInitCountDB()
else
	dbug("Database loaded.")
end

local hy_version = '7.03.00.01'

local commonPigmentID   = 129032    -- Roseate Pigment
local rarePigmentID     = 129034    -- Sallow Pigment
local podID             = 136926    -- Nightmare Pod

local i_am_milling      = false     -- true when we are in the middle of milling
local mass_milling      = false     -- true when we are mass milling
local current_herb      = 0         -- The herb currently being processed, 0 when we're not doing the thing.
local isPod             = false     -- We just opened a pod.

local startingCommon    = 0
local startingRare      = 0
local startingPods      = 0


local function handle_mass_milling(spell)
    -- Called to figure out if we're mass milling rather than regular milling. Gigantic hack.

    if spell == 'Mass Mill Dreamleaf' then
        dbug(string.format("handle_mass_milling(): Dreamleaf"))
        current_herb = 124102
    elseif spell == 'Mass Mill Aethril' then
        dbug(string.format("handle_mass_milling(): Aethril"))
        current_herb = 124101
    elseif spell == 'Mass Mill Yseralline Seeds' then
        dbug(string.format("handle_mass_milling(): Yseralline Seeds"))
        current_herb = 128304
    elseif spell == 'Mass Mill Starlight Rose' then
        dbug(string.format("handle_mass_milling(): Starlight Rose"))
        current_herb = 124105
    elseif spell == 'Mass Mill Fjarnskaggl' then
        dbug(string.format("handle_mass_milling(): Starlight Rose"))
        current_herb = 124104
    elseif spell == 'Mass Mill Foxflower' then
        dbug(string.format("handle_mass_milling(): Starlight Rose"))
        current_herb = 124103
    else
        dbug(string.format("handle_mass_milling(): Spell [%s] not trapped.", spell))
    end

    -- If we don't capture something above, pass through.
    if current_herb ~= 0 then
        i_am_milling = true
        mass_milling = true
        startingCommon = GetItemCount(commonPigmentID, false)
        startingRare = GetItemCount(rarePigmentID, false)
        startingPods = GetItemCount(podID, false)
        dbug(string.format("handle_mass_milling(): Starting common / rare / pod : [%d / %d / %d]", startingCommon, startingRare, startingPods))
    end

end

local function handle_spellcast(self, evt, ...)
    dbug("handle_spellcast()", "Handling spellcast ...")
    
    local unit, spell = ...
    
    if unit == 'player'then
        dbug(string.format("handle_spellcast(): player spell = [%s]", spell))

        if (spell == 'Milling') then
            dbug("handle_spellcast(): Starting milling on current player.")
            i_am_milling = true
            mass_milling = false
        elseif (spell == 'Nightmare Pod') then
            dbug("handle_spellcast(): Opening Nightmare Pod.")
            i_am_milling = true
            mass_milling = false
            current_herb = 136926
            startingCommon = GetItemCount(commonPigmentID, false)
            startingRare = GetItemCount(rarePigmentID, false)
            startingPods = GetItemCount(podID, false)
            isPod = true
        else
            --- If not milling, it might be mass milling. Pass it on to find out.
            handle_mass_milling(spell)
        end
    end
end

local function handle_item_unlocked(self, evt, ...)
    dbug("handle_item_unlocked(): Handling inventory change ...")

    if i_am_milling then
        if current_herb == 0 then
            local bagID, itemSlot = ...

            dbug(string.format("handle_item_unlocked(): Bag [%d] item [%d]", bagID, itemSlot))

            local itemLink = GetContainerItemLink(bagID, itemSlot)
            dbug(string.format("handle_item_unlocked(): ItemLink: %s", itemLink))

            local itemID = GetContainerItemID(bagID, itemSlot)
            dbug(string.format("handle_item_unlocked(): ItemID: %d", itemID))

            local itemName = GetItemInfo(itemID)
            dbug(string.format("handle_item_unlocked(): itemName: [%s]", itemName))
            
            local is_valid = hy_getPigment(itemID)
            
            if is_valid then
                dbug(string.format("Found match for: >>%s<<. Processing.", itemName))
                -- Snapshot what just changed, we'll use this for reference in the bag update handler.
                current_herb = tonumber(itemID)
                startingCommon = GetItemCount(commonPigmentID, false)
                startingRare = GetItemCount(rarePigmentID, false)
                startingPods = GetItemCount(podID, false)

                dbug(string.format("Starting common / rare / pods: [%d / %d / %d]", startingCommon, startingRare, startingPods))
            end
        end
    end
end

local function handle_bag_update(self, evt, ...)

    dbug(string.format("handle_bag_update(): Bag update"))

    if i_am_milling then

        dbug(string.format("handle_bag_update(): I'm milling it."))
        dbug(string.format("handle_bag_update(): Current herb : [%d]", current_herb))

        if current_herb ~= 0 then
            dbug(string.format("handle_bag_update(): Figuring herb out ..."))

            local currentCommon = GetItemCount(commonPigmentID, false, false)
            local currentRare   = GetItemCount(rarePigmentID, false, false)
            local currentPods   = GetItemCount(podID, false, false)

            local newCommon = currentCommon - startingCommon
            local newRare   = currentRare - startingRare
            local newPod    = currentPods - startingPods

            if (newPod < 0) then newPod = 0 end

            dbug(string.format("handle_bag_update(): Yields: current common / rare / pod: [%d / %d / %d]", currentCommon, currentRare, currentPods))
            dbug(string.format("handle_bag_update(): Yields: new common / rare / pod: [%d / %d / %d]", newCommon, newRare, newPod))

            if (newCommon > 0) or (newRare > 0) or (newPod > 0)then

                if isPod then
                    newPod = 0
                end

                dbug(string.format("handle_bag_update(): Yields: common / rare / pod: [%d / %d / %d]", newCommon, newRare, newPod))
                hyUpdateCountDB(current_herb, newCommon, newRare, newPod, mass_milling)
                i_am_milling = false
                mass_milling = false
                current_herb = 0
                isPod = false
            end
        end
    end
end

local function handleEvent(self, evt, ...)
	if evt == 'PLAYER_ENTERING_WORLD' then
		dbug("Handling 'Entering world' Event.")
        hyFrame:RegisterEvent('UNIT_SPELLCAST_SUCCEEDED')
        hyFrame:RegisterEvent('ITEM_UNLOCKED')
        hyFrame:RegisterEvent('BAG_UPDATE_DELAYED')
    elseif evt == 'UNIT_SPELLCAST_SUCCEEDED' then
        handle_spellcast(self, evt, ...)
    elseif evt == 'ITEM_UNLOCKED' then
        handle_item_unlocked(self, evt, ...)
    elseif evt == 'BAG_UPDATE_DELAYED' then
        handle_bag_update(self, evt, ...)
	elseif evt == "PLAYER_LOGIN" then
        print(string.format("|cffffd700*** HerbYield|r %s by |cff66ccffGrimmtooth|r loaded", hy_version))
    else
        print(string.format("*** Untrapped Event [%s]", evt))
	end

end

hyFrame:SetScript('OnEvent', handleEvent)

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--
-- eof
