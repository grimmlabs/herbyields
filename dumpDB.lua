--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--
-- Grimmlabs dbDump - dump a list of herbage stats
--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--

local nameTable = {
	[124101]    = "Aethril",
    [151565]    = "Astral Glory",
	[124102]    = "Dreamleaf",
    [124106]    = "Felwort",
	[124104]    = "Fjarnskaggl",
	[124103]    = "Foxflower",
	[124105]    = "Starlight Rose",
    [128304]    = "Yseralline Seed",
	[136926]	= "Nightmare Pod"
}

local function getName(term)
	local t
	local candidate
	
	for t, candidate in pairs(nameTable) do
		if term == t then
			return candidate
		end
	end
end

SlashCmdList.HERBYIELD_DUMP = function()

    -- This is a gigantic hack to make sure we print ordered
    -- because I fail at tables in Lua.
    local sorted = {
        124101, -- Aethril
        151565, -- Astral Glory
        124102, -- Dreamleaf
        124106, -- Felwort
        124104, -- Fjarnskaggl
        124103, -- Foxflower
        124105, -- Starlight Rose
        128304, -- Yseralline Seed
        136926  -- Nightmare Pod
    }

    print("\n")
	print("            Herb | Samples | Common | Rare | Pods")
	print("=================================================")

    for i=1, table.getn(sorted) do

        local entry = hy_countBase[sorted[i]]
        local samples, common, rare, pod = unpack(entry)

        if samples ~= 0 then
            local itemName = getName(sorted[i])
            print(string.format(" %15s | %7d | %6d | %4d | %4d", itemName, samples, common, rare, pod))
        end
    end

	print("=================================================")
	print("\n")

end

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--
-- eof
