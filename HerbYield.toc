## Interface: 70300
## Title: HerbYield
## Notes: Gathers Milling stats (pigments per mill) automatically for current expansion
## Author: Grimmtooth
## eMail: grimmtooth@gmail.com
## URL: https://bitbucket.org/grimmtooth/herbyields
## Version: 7.03.00.01
## LoadOnDemand: 0
## DefaultState: enabled
## SavedVariables: hy_countBase
	
debug.lua
herbDB.lua
countDB.lua
dumpDB.lua

HerbYield.lua
