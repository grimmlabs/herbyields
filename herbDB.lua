--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--
-- Herb truth table
--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--

local herbList = {
	124101,	-- Aethril
	124102,	-- Dreamleaf
	124103,	-- Foxflower
	124104,	-- Fjarnskaggl
	124105,	-- Starlight Rose
	124106,	-- Felwort
    128304, -- Yseralline Seed
	136926,	-- Nightmare Pod
    151565  -- Astral Glory
}

function hy_getPigment(herb)
	-- print(string.format("> hy_getPigment() - Processing %d", herb))

    for key, value in pairs(herbList) do
        -- print(string.format("> hy_getPigment() - Test value is [%d]", value))
    
        if tonumber(value) == tonumber(herb) then
            -- print(string.format("> hy_getPigment() - hy_getPigment(): Matched!"))
            return true
        end
    end

    dbug(string.format("hy_getPigment(): No match found for [%d]!", herb))
    return false
end
