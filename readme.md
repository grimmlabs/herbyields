# HerbYield #
This program is a World of Warcraft addon.  It has one purpose: to record the yield of pigments from
herbs that your scribe is milling.  It keeps a running total of number of attempts and number of
pigments since the day you installed it.

# Limitations #
This program is limited to herbs that are milled in the current expansion. Previous expansions are of no
concern to me. 

# Usage #
Install it, and it will eavesdrop on your milling activities.

If upgrading from an older version, please read `upgrade.md` before proceeding.

Type `/hd` or `/hydump` into your chat window and it will show you the current running total.

# Installation #
Copy the contents of this directory into your `$WorldOfWarcraftHomeDirectory\interface\addons` directory 
as a new directory called `HerbYield`. Well, actually, you can call the directory anything you want, but 
it'll show up as 'HerbYield' in  your addons menu, so why make it hard on yourself?  Either way, it's your 
choice. I'm not the boss of you.

Alternatively, you can check this repo out directly into your WoW addons directory, if you know what you're 
doing. It'll work.

# Disclaimer #
It works on my machine. Anything else is gravy. If it doesn't work on your machine, you have my sympathies.
And little else. Content yourself with visiting [my blog](http://notallhunters.wordpress.com) and reading the 
results I gathered instead.

# Design Notes - Legion #
Legion moved away from the single pigment model of WoD back to a common / rare model, so I had to undo
the shortsighted changes I made previously in that regard. Additionally, Legion adds a number of
new secondary yields such as Yseralline Seeds, Nightmare Pods, and crystal fragements. After
thinking about it (and partially implementing something to count those up too) I decided that what
really interests me is the pigment I harvest, not the secondary stuff.  True, the seeds also 
mill down, and Pods do yield up pigments, but I'm not sure how interesting those specs would be
when deciding which herb to farm or buy on the AH.  In the end, the pigments rule. 

After a while, however, I made a decision to capture the yields from opening Nightmare Pods. What this does
is reveal that Dreamleaf is a lot more effective than its base yields suggest. It's so good that Dreamleaf
is almost always the right choice when asking "what should I buy / harvest?" because the Roseate Pigment yield
of the Pods is phenomenal. So basically you need to factor in the controbution of Pods with the contribution
of Dreamleaf itself.  