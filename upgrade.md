# Upgrade Instructions #

If upgrading from a previous version, please go to your
`$WorldOfWarcraftHomeDirectory\WTF\Account\$YourAccountName\SavedVariables`
directory and delete the files `HerbYield.lua` and `HerbYield.lua.bak`
before running the addon again.
