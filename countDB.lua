--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--
-- Herb count database
--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--

-- Format:
-- [herbID] = {[attempts], [common], [rare], [pod}
local local_countBase = {
	[124101]    = {0,0,0,0},  -- Aethril
	[124102]    = {0,0,0,0},  -- Dreamleaf
	[124103]    = {0,0,0,0},  -- Foxflower
	[124104]    = {0,0,0,0},  -- Fjarnskaggl
	[124105]    = {0,0,0,0},  -- Starlight Rose
    [124106]    = {0,0,0,0},  -- Felwort
    [128304]    = {0,0,0,0},  -- Yseralline Seed
	[136926]	= {0,0,0,0},  -- Nightmare Pod
	[151565]	= {0,0,0,0}	  -- Astral Glory
}

function hyInitCountDB()
	dbug("hyInitCountDB(): Initializing database.")
    hy_countBase = local_countBase
end


function hyUpdateCountDB(herbID, commonCount, rareCount, podCount, massMilling)
    dbug("hyUpdateCountDB(): Updating count database")
	local herb = tonumber(herbID)
	local cCount = tonumber(commonCount)
    local rCount = tonumber(rareCount)
	local pCount = tonumber(podCount)

	dbug(string.format("hyUpdateCountDB(): Updating HerbID [%d] with [%d / %d / %d] items", herb, cCount, rCount, pCount))
	
	local count, dbcommon, dbrare, dbpod = unpack(hy_countBase[herb])
	dbug(string.format("hyUpdateCountDB(): Start = [%d / %d / %d / %d]", count, dbcommon, dbrare, dbpod))

	if massMilling == true then
		-- mass milling does stacks of 20, not stacks of 5
		count = count + 4
	else
		count = count + 1
	end

	dbcommon = dbcommon + cCount
    dbrare = dbrare + rCount
	dbpod = dbpod + pCount

	hy_countBase[herb] = {count, dbcommon, dbrare, dbpod}
	dbug(string.format("Finish = [%d / %d / %d / %d]", unpack(hy_countBase[herb])))
end

